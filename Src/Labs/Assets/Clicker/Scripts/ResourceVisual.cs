using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class ResourceVisual : MonoBehaviour
{
    
    private TextMeshProUGUI _resourceVis;
    private ResourceBank _resourceBank;
    private GameResource _resources;

    private void Start()
    {
        _resourceVis.text = _resourceBank.GetResource(_resources).ToString();
    }

    private void Update()
    {
        ChangeTxt( _resourceBank.GetResource(_resources));
    }

    public void ChangeTxt( int Resource)
    {
        _resourceVis.text = Resource.ToString();
    }
}
