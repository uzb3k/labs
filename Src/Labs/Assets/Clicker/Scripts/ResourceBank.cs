﻿using System.Collections.Generic;
using UnityEngine;

public class ResourceBank : MonoBehaviour
{
    private ResourceVisual _resourceVisual;
    private Dictionary<GameResource, int> _resources;
    public static ResourceBank intance;
    
    private void Awake()
    {
        
        _resources = new Dictionary<GameResource, int>()
        {
            [GameResource.FOOD] = Game.Intance.GameConfig.Food,
            [GameResource.HUMANS] = Game.Intance.GameConfig.Humans,
            [GameResource.WOOD] = Game.Intance.GameConfig.Wood,
            [GameResource.STONE] = Game.Intance.GameConfig.Stone,
            [GameResource.GOLD] = Game.Intance.GameConfig.Gold
        };
    }

    public void ChangeResource(GameResource r, int v)
    {
        if (!_resources.ContainsKey(r))
        {
            _resources.Add(r, v);
        }
        _resources[r] += v;
        _resourceVisual.ChangeTxt(_resources[r]);
    }

    public int GetResource(GameResource r)
    {
        return _resources[r];
    }
}