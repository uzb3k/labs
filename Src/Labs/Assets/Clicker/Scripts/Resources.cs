﻿using UnityEngine;
   public enum GameResource
    {
        HUMANS,
        FOOD,
        WOOD,
        STONE,
        GOLD
    }
