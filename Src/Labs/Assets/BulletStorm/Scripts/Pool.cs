using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    [SerializeField] private PoolConfig _poolConfig;
    [SerializeField] private List<GameObject> _poolObjects;
    [SerializeField] private Transform _objectsRoot;

    private void Awake()
    {
        InitializePoolItems();
    }

    private void InitializePoolItems()
    {
        foreach (PoolItem item in _poolConfig.PoolItems)
        {
            for (int i = 0; i < item.Count; i++)
            {
                GameObject instantiatedItem = Instantiate(item.Prefab, _objectsRoot);
                _poolObjects.Add(instantiatedItem);
                instantiatedItem.SetActive(false);
            }
        }
    }

    public GameObject GetPoolItem(string tag)
    {
        foreach (GameObject item in _poolObjects)
        {
            if (item.tag.Equals(tag) && !item.gameObject.activeInHierarchy)
            {
                item.SetActive(true);
                return item;
            }
        }
        return null;
    }

    public void ReturnToPool(GameObject poolItem) => 
        poolItem.SetActive(false);
}
