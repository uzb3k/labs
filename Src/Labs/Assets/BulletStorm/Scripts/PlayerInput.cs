using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private Camera _mainCamera;
    
    private void Update()
    {
        GetScreenPoint();
    }

    private Vector3 GetScreenPoint()
    {
        if (Input.GetMouseButton(0))
        {
            return _mainCamera.ScreenToWorldPoint(Input.mousePosition);
        }
        return Vector3.zero;
    }
}
