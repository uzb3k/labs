using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soud : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _audioClip;

    public void PlayClickSound()
    {
        if (!_audioSource.isPlaying)
        {
            _audioSource.clip = _audioClip;
            _audioSource.Play();
        }
    }
}
