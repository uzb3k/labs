using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;


public class InventorInteracable : MonoBehaviour, OnPointerDownHandler, OnPointerExitHandler, OnPointerEnterHandler
{
    [SerializeField] private Image _invetoryImg;
    public void OnPointerDown(PointerEventData eventData)
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        _invetoryImg.transform.localScale = new Vector2(1.5f, 1.5f);
        
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        
    }
}

internal interface OnPointerEnterHandler
{
}

internal interface OnPointerExitHandler
{
}

internal interface OnPointerDownHandler
{
}