using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class item : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    private static Transform _dragFrom;
    public static item DragItem;

    [SerializeField] private SoundOnClick _playSound;
    [SerializeField] private Image _slotImage;

    private float _scaleSize = 1.5f;

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Mouse0) & DragItem != null)
        {
            Drop(_dragFrom);
        } 
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        _slotImage.raycastTarget = false;
        _dragFrom = transform.parent;
        DragItem = this;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        _slotImage.raycastTarget = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _playSound.PlaySound();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        transform.localScale = Vector2.one * _scaleSize;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        transform.localScale = Vector2.one;
    }

    private void Drop(Transform transform)
    {
        DragItem.transform.SetParent(transform);
        DragItem.transform.localPosition = Vector2.zero;
        _dragFrom = null;
        DragItem = null;

    }


}
